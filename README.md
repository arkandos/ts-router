This is a simple yet powerful routing library with bindings for various frameworks that I use.

# Features

* 🚀 **Tiny** - small 300LOC core library, ~2k minified+gzipped in production, including framework integrations
* 🤘 **Well-tested** - Lots of tests for the core functionality, Used in hundreds of websites.
* 🤩 **Powerful** supports custom patterns, backtracking, capturing arrays, and more!

# Quick Start

## Node.JS / Server-side

```bash
npm install @jreusch/router-node
```

Even though it says `Node`, it does not use any Node-specific APIs and is fully generic in its types. You can plug it in to basically anything, even client-side code!

Head over to the [Package documentation](packages/node/) to learn about the API!

## Vue:

Only Vue 3 is supported.

```bash
npm install @jreusch/router-vue
```

Then head over to the [Vue documentation](packages/vue/) to learn about the available components!

## Preact:


```bash
npm install @jreusch/router-preact
```

Then head over to the [Preact documentation](packages/preact/) to learn about the available components and hooks!

## React:

I've never really used "true" React, so this is almost the same as the Preact version, but changes the imports, and uses the new `useSyncExternalStore` hook to subscribe to URL changes. If you notice anything different, please tell me!

```bash
npm install @jreusch/router-react
```

Then head over to the [Preact documentation](packages/react/) to learn about the available components and hooks!

## Build something new:

Of course, you can also start from the [core library](packages/core/) to make your own integration:

```bash
npm install @jreusch/router
```


# Contributing

If you found a bug, please [submit an issue](https://gitlab.com/arkandos/ts-router/-/issues/new), including the framework and library versions you used, and if possible, a [SSCCE](http://sscce.org/) reproducing your issue. In general, I believe that these libraries are very well tested and should not break during normal usage; If you still manage to do that, or find a new edge-case, I'm very happy to hear about it!

I'm always excited about new frameworks, so if you want to contribute a binding, please feel free to do so! One goal of this library is to always provide an API that feels _same-y_, with only small framework-specific differences, usually motivated by different conventions; I'd like to provide _same-y_ APIs for new library, whenever possible.

Being able to make the library smaller is always exciting. If you have ideas, don't hesitate to share them with me, even if that includes changing the whole API. Just go for it!

If you have suggestions for new features, please take a moment to consider the implications of your idea for the overall design goals of this library. I don't want to make a feature-complete replacement for `vue-router`, `react-router`, and whatnot. Instead, this is a `80%` solution to the problem, while still being as small and powerful as possible. If what you want is available there, strongly consider using their implementation instead. On the other hand, tree-shaking (dead code elimination) works pretty well nowadays, so additional functions should not be a problem.

In the end, all I want to do is to display different content depending on what's in the URL bar.


# Support / Climate action

This library was made with ☕, 💦 and 💜 by [joshua](https://joshi.monster)
If you really like what you see, you can [Buy me more ☕](https://www.paypal.com/donate/?hosted_button_id=CNT7EUVAM6MZS), or [get in touch](jreusch4+oss@gmail.com)!

If you work on limiting climate change, preserving the environment, et al. and any of my software is useful to you, please let me know if I can help and prioritize issues!

