import { createMemoryRouter, RouterUrl } from "@jreusch/router";
import { createElement, useState, ReactNode } from "react";
import Router from "./Router";

export default function MemoryRouter(props: { children: ReactNode, onChange?: (newUrl: RouterUrl, oldUrl: RouterUrl) => void }) {
    const [router] = useState(createMemoryRouter())
    return <Router children={props.children} router={router} onChange={props.onChange} />
}
