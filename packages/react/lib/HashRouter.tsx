import { createHashRouter, RouterUrl } from "@jreusch/router";
import { createElement, useState, ReactNode } from "react";
import Router from "./Router";

export default function HashRouter(props: { children: ReactNode, onChange?: (newUrl: RouterUrl, oldUrl: RouterUrl) => void }) {
    const [router] = useState(createHashRouter())
    return <Router children={props.children} router={router} onChange={props.onChange} />
}
