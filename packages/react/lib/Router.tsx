import type { Router, RouterUrl } from "@jreusch/router"
import { ReactNode, createElement, createContext, useContext, useEffect, useRef, useState, useSyncExternalStore } from "react"

export interface RouterContext {
    url: RouterUrl
    matched: string
    router: Router
}


function createRouterContext(router: Router): RouterContext {
    return {
        url: router.getUrl(),
        matched: '',
        router
    }
}

const Context = createContext<RouterContext|null>(null)

export function useRouterContext() {
    return useContext(Context)!
}

/**
 * Provides access to the router object for programmatic navigation.
 *
 * Only available inside of a `<Router>` component tree.
 */
export function useRouter(): Router {
    return useRouterContext().router
}


/**
 * Subscribe to URL changes.
 */
 export function useCurrentUrl(): RouterUrl {
    return useRouterContext().url
}


export interface RouterProps {
    children: ReactNode

    /**
     * Router to use.
     * Needs to be provided to support bundle-splitting.
     */
    router: Router,

    /**
     * Optional callback to listen to URL changes.
     */
    onChange?: (to: RouterUrl, from: RouterUrl) => any
}

/**
 * Top-level component; should wrap all `<Route>` components in some way.
 * It is ensured that only 1 pattern can match inside a `<Router>` tree.
 */
export default function Router(props: RouterProps) {
    const onChange = useRef(props.onChange)
    useEffect(() => {
        onChange.current = props.onChange
    }, [props.onChange])

    const [context, setContext] = useState(() => createRouterContext(props.router))

    const url = useSyncExternalStore(props.router.subscribe, props.router.getUrl)
    const previousUrl = useRef(url)

    useEffect(() => {
        setContext(createRouterContext(props.router))
    }, [props.router])

    useEffect(() => {
        setContext(context => createRouterContext(context.router))
        if (onChange.current) {
            onChange.current(url, previousUrl.current)
        }
        previousUrl.current = url
    }, [url])

    return <Context.Provider value={context} children={props.children} />
}
