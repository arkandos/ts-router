import { resolve } from 'path'
import { defineConfig } from "vite"
import vue from '@vitejs/plugin-vue'
import dts from 'vite-plugin-dts'

export default defineConfig({
    plugins: [
        vue(),
        dts()
    ],

    build: {
        lib: {
            entry: resolve(__dirname, 'lib/index.ts'),
            name: 'Router',
            fileName: 'router'
        },
        rollupOptions: {
            external: ['vue'],
            output: {
                globals: {
                    'vue': 'Vue',
                }
            }
        }
    },
})
