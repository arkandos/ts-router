import { Router } from "@jreusch/router"
import { inject, provide, ref, watchEffect, Ref, InjectionKey } from "vue"
import { MaybeComputedRef, resolveUnref } from "./resolveUnref"

export interface RouterContext {
    matched: string
    router: Router
    url: Ref<string|null>
}

const symbol: InjectionKey<RouterContext> = Symbol('router')

export function useRouterContext(): RouterContext {
    return inject(symbol)!
}

/**
 * Use the provided router, to allow programmatic navigation.
 */
export function useRouter(): Router {
    return useRouterContext().router
}

/**
 * Provide the current URL used in the router as a ref.
 */
export function useCurrentUrl(): Ref<string|null> {
    return useRouterContext().url
}

export function provideRouterContext(router: MaybeComputedRef<Router>): RouterContext {
    const ctx: RouterContext = {
        matched: '',
        router: resolveUnref(router),
        url: ref(null)
    }

    watchEffect(onCleanup => {
        ctx.matched = ''
        ctx.router = resolveUnref(router)
        ctx.url.value = ctx.router.getUrl()

        onCleanup(ctx.router.subscribe(newUrl => {
            ctx.matched = ''
            ctx.url.value = newUrl
        }))
    })

    provide(symbol, ctx)

    return ctx
}
