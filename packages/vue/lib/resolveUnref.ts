import { unref, ComputedRef, Ref } from 'vue'

// copied from vueuse
type MaybeReadonlyRef<T> = (() => T) | ComputedRef<T>
type MaybeRef<T> = T | Ref<T>
export type MaybeComputedRef<T> = MaybeReadonlyRef<T> | MaybeRef<T>

export function resolveUnref<T>(r: MaybeComputedRef<T>): T {
    return typeof r === 'function'
      ? (r as any)()
      : unref(r)
}
