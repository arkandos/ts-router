import { match, Params, parse } from "@jreusch/router"
import { computed, Ref } from "vue"
import { useCurrentUrl } from "./context"
import { resolveUnref, MaybeComputedRef } from './resolveUnref'


/**
 * Lower-level hook to match a pattern against the current URL.
 */
export default function useMatch(pattern: MaybeComputedRef<string>, allowPartial: MaybeComputedRef<boolean> = false): Ref<Params|null> {
    const currentUrl = useCurrentUrl()
    const parsedPattern = computed(() => parse(resolveUnref(pattern)))
    return computed(() => {
        const url = currentUrl.value
        if (url) {
            return match(parsedPattern.value, url, resolveUnref(allowPartial))
        } else {
            return null
        }
    })
}
