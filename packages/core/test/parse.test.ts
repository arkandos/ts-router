import { describe, expect, it } from 'vitest'

import { parse } from "../src/"


describe('parse', () => {
    it('parses empty strings to an empty value', () => {
        expect(parse('')).toStrictEqual([])
    })

    it('parses a single slash to an empty value', () => {
        expect(parse('/')).toStrictEqual([])
    })

    it('parses empty segments with optional/repeated as match-all', () => {
        expect(parse('/*')).toStrictEqual([
            {
                optional: true,
                repeated: true
            }
        ])
        expect(parse('/+')).toStrictEqual([
            {
                optional: false,
                repeated: true
            }
        ])
        expect(parse('/?')).toStrictEqual([
            {
                optional: true,
                repeated: false
            }
        ])
    })

    it('parses static url patterns', () => {
        expect(parse('/hello/world')).toStrictEqual([
            {
                str: 'hello'
            },
            {
                str: 'world'
            }
        ])

        expect(parse('/this-is/A path Wi%20th special #stuff')).toStrictEqual([
            {
                str: 'this-is'
            },
            {
                str: 'a path wi th special #stuff'
            }
        ])
    })

    it('parses static url patterns with modifiers', () => {
        expect(parse('/test*')).toStrictEqual([
            {
                str: 'test*'
            }
        ])

        expect(parse('/test?')).toStrictEqual([
            {
                str: 'test?'
            }
        ])

        expect(parse('/test+')).toStrictEqual([
            {
                str: 'test+'
            }
        ])

        expect(parse('/test*this?should+work')).toStrictEqual([
            {
                str: 'test*this?should+work'
            }
        ])
    })

    it('ignores a single slash at the end', () => {
        expect(parse('/test/')).toStrictEqual([
            {
                str: 'test'
            }
        ])
    })

    it('parses basic variables', () => {
        expect(parse('/:slug')).toStrictEqual([
            {
                name: 'slug',
            }
        ])

        expect(parse('/users/:userId')).toStrictEqual([
            {
                str: 'users'
            },
            {
                name: 'userId',
            }
        ])
    })

    it('parses basic variables with modifiers', () => {
        expect(parse('/users/:userId?')).toStrictEqual([
            {
                str: 'users'
            },
            {
                name: 'userId',
                optional: true,
                repeated: false,
            }
        ])

        expect(parse('/:path+')).toStrictEqual([
            {
                name: 'path',
                repeated: true,
                optional: false
            }
        ])

        expect(parse('/:anything*')).toStrictEqual([
            {
                name: 'anything',
                optional: true,
                repeated: true,
            }
        ])
    })

    it('fails if there is stuff after modifiers', () => {
        expect(() => parse('/:test*more')).toThrowError()
        expect(() => parse('/:test*?')).toThrowError()
    })

    it('empty variables are required wildcards for a single segment', () => {
        expect(parse('/:')).toStrictEqual([{name: ''}])
        expect(parse('/test/:')).toStrictEqual([{str: 'test'}, {name: ''}])
        expect(parse('/:*')).toStrictEqual([{optional: true, repeated: true, name: ''}])
    })

    it('parses variables with custom regex patterns', () => {
        expect(parse('/:id([0-9]+)')).toStrictEqual([
            {
                name: 'id',
                regex: /^[0-9]+$/i,
                str: undefined,
            }
        ])

        expect(parse('/:id(([0-9])+)')).toStrictEqual([
            {
                name: 'id',
                regex: /^([0-9])+$/i,
                str: undefined
            }
        ])
    })

    it('parses variables with custom regex patterns and modifiers', () => {
        expect(parse('/:id([0-9]+)?')).toStrictEqual([
            {
                name: 'id',
                regex: /^[0-9]+$/i,
                str: undefined,
                optional: true,
                repeated: false
            }
        ])
        expect(parse('/:id([0-9]+)+')).toStrictEqual([
            {
                name: 'id',
                regex: /^[0-9]+$/i,
                str: undefined,
                repeated: true,
                optional: false
            }
        ])
        expect(parse('/:id([0-9]+)*')).toStrictEqual([
            {
                name: 'id',
                regex: /^[0-9]+$/i,
                str: undefined,
                optional: true,
                repeated: true,
            }
        ])
    })

    it ('empty custom regex patterns are ignored', () => {
        expect(parse('/:id()')).toStrictEqual([{name: 'id', str: undefined }])
    })

    it ('fails on unexpected stuff after regex patterns', () => {
        expect(() => parse('/:id([0-9]+)x/here')).toThrowError()
        expect(() => parse('/:id([0-9]+)?more/here')).toThrowError()
    })
})
