import { it, describe, expect } from "vitest";
import { parse, stringify } from "../src";

describe('stringify', () => {
    it('echos simple patterns', () => {
        const patterns = [
            '/',
            '/hello/world',
            '/something-wicked',
            '/this%20is%20more%20complicated'
        ]

        for (const pattern of patterns) {
            expect(stringify(parse(pattern), {})).toBe(pattern)
        }
    })

    it('replaces variables', () => {
        expect(stringify(parse('/user/:userId'), {userId: '1234'})).toBe('/user/1234')
    })

    it('replaces array variables', () => {
        expect(stringify(parse('/:slug+/edit'), {
            slug: ['something', 'wicked']
        })).toBe('/something/wicked/edit')
    })

    it('ignores missing vaiables', () => {
        expect(stringify(parse('/user/:userId/edit'), {})).toBe('/user/edit')
    })

    it('encodes weird param values', () => {
        expect(stringify(parse('/:slug+/edit'), {
            slug: ['hello world']
        })).toBe('/hello%20world/edit')

        expect(stringify(parse('/user/:userId'), { userId: ':userId'}))
            .toBe('/user/%3AuserId')
    })

})

