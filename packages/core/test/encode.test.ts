import { it, describe, expect } from 'vitest'
import { encode } from '../src'

describe('encode', () => {
    const colon = ':'.charCodeAt(0).toString(16).toUpperCase()
    const star = '*'.charCodeAt(0).toString(16).toUpperCase()
    const plus = '+'.charCodeAt(0).toString(16).toUpperCase()

    it('does nothing for simple segments', () => {
        const patterns = [
            '/',
            '/hello/world',
            'something-wicked',
            '/this%20is%20more%20complicated'
        ]

        for (const pattern of patterns) {
            expect(encode(pattern)).toBe(pattern)
        }
    })

    it('escapes variables', () => {
        expect(encode('/:title')).toBe(`/%${colon}title`)
        expect(encode('/:a/:b/:c')).toBe(`/%${colon}a/%${colon}b/%${colon}c`)
    })

    it('escapes wildcards', () => {
        expect(encode('/:')).toBe(`/%${colon}`)
        expect(encode('/*')).toBe(`/%${star}`)
        expect(encode('/+')).toBe(`/%${plus}`)

        expect(encode('/blog/*')).toBe(`/blog/%${star}`)
        expect(encode('/user/:/blog/+')).toBe(`/user/%${colon}/blog/%${plus}`)
    })
})
