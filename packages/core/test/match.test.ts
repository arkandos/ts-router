import { describe, expect, it } from "vitest"

import { parse, match } from '../src/'

describe('match', () => {
    const start = parse('/')
    const wildcard = parse('/*')
    const simple = parse('/about/me/')
    const variables = parse('/user/:userId/edit/:slug*')
    const regex = parse('/user/:userId([0-9]+)')
    const backtrack = parse('/:slug*/edit')

    it('does not match additional things at the end', () => {
        expect(match(start, '/')).not.toBeNull()
        expect(match(start, '/x')).toBeNull()
        expect(match(start, '/x/')).toBeNull()
    })

    it('wildcards match anything', () => {
        expect(match(wildcard, '/')).not.toBeNull()
        expect(match(wildcard, '/x')).not.toBeNull()
        expect(match(wildcard, '/x/')).not.toBeNull()
        expect(match(wildcard, '/x/more/stuff')).not.toBeNull()
    })

    it('matches simple paths', () => {
        expect(match(simple, '/about/me')).not.toBeNull()
        expect(match(simple, '/')).toBeNull()
        expect(match(simple, '/about/me-and-you')).toBeNull()
        expect(match(simple, '/about/me ')).toBeNull()
        expect(match(simple, '/x/about/me')).toBeNull()
        expect(match(simple, '/about/me/x')).toBeNull()
    })

    it('ignores case', () => {
        expect(match(simple, '/about/me')).not.toBeNull()
        expect(match(simple, '/About/Me')).not.toBeNull()
        expect(match(simple, '/ABOUT/ME')).not.toBeNull()
    })

    it('matches variables and returns them as an object', () => {
        expect(match(variables, '/user/1234/edit/my-account')).toStrictEqual({
            userId: '1234',
            slug: ['my-account']
        })
    })

    it('matches regex variables, but only if the regex matches', () => {
        expect(match(regex, '/user/1234')).toStrictEqual({
            userId: '1234'
        })

        expect(match(regex, '/user/asdf')).toBeNull()
    })

    it('backtracks wildcards if needed', () => {
        expect(match(backtrack, '/something/some-post/edit')).toStrictEqual({
            slug: ['something', 'some-post']
        })
        expect(match(backtrack, '/something/edit/edit')).toStrictEqual({
            slug: ['something', 'edit']
        })
    })

    // TODO: this might be unexpected:
    //
    // match(parse('/:slug*'), '/hello/world', false) ==> {slug: ['hello','world']}
    // match(parse('/:slug*'), '/hello/world', true) ==> {slug: ['hello']}
    //
    //      ... and then early out becaues we already match the entire pattern
    //
    // this does exactly what it says it should, but is that correct?
    // I don't know right now!

    // TODO: Write some tests showing that immutable updates are really necessary in addParam

    // TODO: this might be unexpected:
    // match(parse('/:a?/:b+'), '/a') ==> null
    // since a? greedily consumes /a, and then :b+ fails since it is not optional.
})
