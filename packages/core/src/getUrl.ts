import encode from "./encode";
import parse from "./parse";
import stringify from "./stringify";
import { Params } from "./types";

/**
 * If params are provided, parse and stringify the url as a pattern.
 * Otherwise, encode the url.
 */
export default function getUrl(url: string, params?: Params): string {
    return params ? stringify(parse(url), params) : encode(url)
}
