

export type Params = Record<string, string | string[]>

export interface Segment {
    name?: string
    str?: string
    optional?: boolean
    repeated?: boolean
    regex?: RegExp
}

export type Pattern = Segment[]


export function decode (c: string) {
    return decodeURIComponent(c).toLowerCase()
}

export function fail (c: string) {
    throw new Error(c)
}

export const SEPARATOR = '/'
