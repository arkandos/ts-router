
/**
 * Encodes a URL such that it is safe to use as a pattern.
 * Does NOT perform or replace encodeURI(Component)!
 *
 * Currently, this involves only replacing '/:' with '/%3a' to make sure
 * all segments will be parsed as static segments.
 *
 * If your URL was encoded using encodeURIComponent already, it is
 * not necessary to call this function.
 *
 * @param url a URL from anywhere
 * @returns an escaped URL, safe to use in a pattern.
 */
export default function encode(url: string): string {
    return url
        .replaceAll('/:', '/%3A')
        .replaceAll('/*', '/%2A')
        .replaceAll('/+', '/%2B')
        // we do not need to escape /? since it cannot happen in URLs
}
