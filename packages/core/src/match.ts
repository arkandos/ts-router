import { decode, Params, Pattern, Segment, SEPARATOR } from "./types"

/**
 * Match a url against a pattern.
 *
 * @param pattern The pattern to match against
 * @param url The url to test. Slashes (/) at the start and end are stripped.
 * @param allowPartial If set, returns partial matches, where the pattern only matches the start of the url.
 * @returns The params object containing extracted values, or null if the pattern does not match.
 */
export default function match(pattern: Pattern, url: string, allowPartial = false): Params | null {
    const parts = url.split(SEPARATOR).filter(x => x).map(decode)
    return matchRec(pattern, parts, {}, allowPartial)
}


function matchRec(segments: Segment[], parts: string[], params: Params, allowPartial: boolean): Params|null {
    if (!segments.length) {
        // no segments - we match if the path is empty or we allow partial matches
        if (!parts.length || allowPartial) {
            return params
        } else {
            return null
        }
    } else if (!parts.length) {
        // no parts - we match if every remaining segment is optional
        if (segments.every(segment => segment.optional)) {
            return params
        } else {
            return null
        }
    } else {
        const [segment, ...segmentsTail] = segments
        const [part, ...partsTail] = parts

        if (segment.regex
                ? segment.regex.test(part)
                : (!segment.str || segment.str === part)
        ) {
            // try first to consume the segment, if repeatable, fall back to matching again
            // (non-greedy wildcards)
            const newParams = addParam(params, segment, part)
            const match = matchRec(segmentsTail, partsTail, newParams, allowPartial)
            if (match || !segment.repeated) {
                return match
            } else {
                return matchRec(segments, partsTail, newParams, allowPartial)
            }
        } else if (segment.optional) {
            return matchRec(segmentsTail, parts, params, allowPartial)
        } else {
            return null
        }
    }
}

function addParam(params: Params, segment: Segment, part: string): Params {
    const name = segment.name
    if (!name) {
        return params
    } else if (segment.repeated) {
        return { ...params, [name]: [...params[name] || [], part] }
    } else {
        return { ...params, [name]: part }
    }
}
