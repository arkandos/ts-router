import { decode, fail, Pattern, Segment, SEPARATOR } from "./types"

const enum ParserState {
    Start,
    Static,
    Variable,
    End,
    CustomRegex,
    CustomRegexEnd
}

function isAtEnd (c: string|undefined): c is undefined {
    return !c || c === SEPARATOR
}


/**
 * Parse a pattern string into a pattern.
 *
 * See the README for more information about the syntax and semantics.
 *
 * @param s The pattern string
 * @returns The parsed pattern
 */
export default function parse(s: string): Pattern {
    const result: Segment[] = []

    let state = ParserState.Start
    let parens = 1
    let current: Segment = {}

    function finalize () {
        if (current.str) {
            current.str = decode(current.str)
        }

        result.push(current)
        current = {}
        state = ParserState.Start

        return true
    }

    function variableModifiers (c: string) {
        const star = c === '*'
        const plus = c === '+'
        const question = c === '?'

        if (star || plus || question) {
            current.optional = star || question
            current.repeated = star || plus
            state = ParserState.End

            return true
        } else {
            return false
        }
    }

    function variableEnd (c: string|undefined) {
        if (isAtEnd(c)) {
            return finalize()
        } else if (variableModifiers(c)) {
            return true
        } else {
            return false
        }
    }

    // <= to get a value of undefined for the EOS
    for(let i = 0; i <= s.length; i++) {
        const c = s.charAt(i)

        // cast required here because otherwise Typescript tries to be TOO smart
        switch (state as ParserState) {
            case ParserState.Start:
                if (c === SEPARATOR) {
                    break
                } else if (c === ':') {
                    current.name = ''
                    state = ParserState.Variable
                } else if(!variableModifiers(c)) {
                    current.str = c
                    state = ParserState.Static
                }
                break

            case ParserState.Static:
                if (isAtEnd(c)) {
                    finalize()
                } else {
                    current.str += c
                }
                break

            case ParserState.End:
                if (!isAtEnd(c)) {
                    fail(`Unexpected '${c}' after modifiers!`)
                }
                finalize()
                break

            case ParserState.Variable:
                if (c === '(') {
                    current.str = ''
                    parens = 1
                    state = ParserState.CustomRegex
                } else if (!variableEnd(c)) {
                    current.name += c
                }
                break

            case ParserState.CustomRegex:
                if (c === '(') {
                    parens++
                    current.str += c
                } else if (c === ')' && --parens === 0) {
                    if (current.str) {
                        current.regex = new RegExp('^' + current.str + '$', 'i')
                    }

                    current.str = undefined
                    state = ParserState.CustomRegexEnd
                } else {
                    current.str +=  c
                }
                break

            case ParserState.CustomRegexEnd:
                if (!variableEnd(c)) {
                    fail(`Unexpected '${c}' after custom regex`)
                }
                break
        }
    }

    return result
}
