import { addBase, resolve, Router, RouterUrl } from './common'

/**
 * A path-basedd HTML5 History based implementation using pushState, etc.
 * Main implementation used in browsers.
 *
 * @example
 * createPathRouter() // '/hello'
 * createPathRouterWithBase('/base') // '/base/hello'
 */
export function createPathRouter(): Router {
    const getUrl = () => location.pathname
    const toHref = (url: string) => url

    return createHtml5History(getUrl, toHref)
}

/**
 * A path-basedd HTML5 History based implementation using pushState, etc.
 * Main implementation used in browsers.
 *
 * @example
 * createPathRouterWithBase() // '/hello', same as createPathRouter
 * createPathRouterWithBase('/base') // '/base/hello'
 */
export function createPathRouterWithBase(base = '/'): Router {
    base = resolve(location.pathname, base)

    const getUrl = () => {
        if (base === '/') {
            return location.pathname
        } else if (location.pathname.startsWith(base)) {
            return location.pathname.slice(base.length) || '/'
        } else {
            return null
        }
    }

    const toHref = (url: string) => addBase(base, url)

    return createHtml5History(getUrl, toHref)
}

/**
 * An alternative HTML5 history using the hash of the path, but still utilizing
 * pushState, etc. under the hood.
 *
 * Use this if your server does not support catch-all (SPA style) routing.
 */
export function createHashRouter(): Router {
    const getUrl = () => {
        return location.hash.slice(1) || '/'
    }

    const toHref = (url: string) => {
        return '#' + resolve(getUrl(), url)
    }

    return createHtml5History(getUrl, toHref)
}


// subs need to be global, such that navigations from 1 html5 router triggers
// subscriptions in all the other ones.
const subs: Set<() => void> = new Set()
function onUrlChanged() {
    subs.forEach(sub => sub())
}

function createHtml5History(
    getUrl: () => RouterUrl,
    toHref: (url: string) => string
): Router {
    return {
        getUrl,
        toHref,

        subscribe(onUrlChange: (newUrl: RouterUrl) => void): (() => void) {
            if (!subs.size) {
                window.addEventListener('popstate', onUrlChanged)
            }

            const cb = () => onUrlChange(getUrl())

            subs.add(cb)

            return () => {
                subs.delete(cb)
                if (!subs.size) {
                    window.removeEventListener('popstate', onUrlChanged)
                }
            }
        },

        navigate(to: string, replace = false) {
            to = toHref(to)

            if (replace) {
                history.replaceState(null, '', to)
            } else {
                history.pushState(null, '', to)
            }

            onUrlChanged()
        },

        go(delta: number) {
            history.go(delta)
        },
    }
}
