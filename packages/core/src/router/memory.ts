import { addBase, Router, RouterUrl } from './common'

/**
 * An in-memory router implementation, mainly provided for SSR.
 */
export default function createMemoryRouter(base = '/'): Router {
    const history = [base]
    let position = 0

    const subs: Set<(newUrl: RouterUrl) => void> = new Set()
    const onUrlChanged = () => {
        subs.forEach(onUrlChange => onUrlChange(history[position]))
    }

    return {
        getUrl() {
            return history[position]
        },

        toHref(url) {
            return addBase(base, url)
        },

        subscribe(onUrlChange) {
            subs.add(onUrlChange)
            return () => void subs.delete(onUrlChange)
        },

        navigate(url, replace?) {
            if (replace) {
                history.splice(position, 1, url)
            } else {
                history.splice(0, position, url)
            }

            onUrlChanged()
        },

        go(delta) {
            // history is a stack (the first element is the most recent one),
            // so if we want to move back (e.g. -1), we need to increase position
            position -= delta
            if (position < 0) {
                position = 0
            }
            if (position >= history.length) {
                position = history.length - 1
            }

            onUrlChanged()
        }
    }
}
