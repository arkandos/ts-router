
/**
 * A client-side Router is an object that allows access to the current URL,
 * provides client-side navigation, and notifies subscribers whenever the URL
 * canges.
 */
 export interface Router {
    /**
     * Get the current URL that should be used for matching.
     */
    getUrl(): RouterUrl

    /**
     * Turn a url into a value suitable for a link.
     */
    toHref(url: string): string;

    /**
     * Subscribe to URL changes.
     * @param onUrlChange Function to call if URL changes
     * @returns Function to unsubscribe from the changes.
     */
    subscribe(onUrlChange: (newUrl: RouterUrl) => void): () => void;

    /**
     * Navigate somewhere else, programmatically.
     *
     * @param url Url to go to. Should be compatible with values returned by toUrl
     * @param replace If set, replace the current history entry instead of creating a new one.
     */
    navigate(url: string, replace?: boolean): void;

    /**
     * Navigate back and forward through the history.
     *
     * @example
     * router.go(-1) // go back
     * router.go(1) // go forward
     */
    go(delta: number): void;
}


export type RouterUrl = string|null;

export function resolve(base: string, path: string): string {
    if (path[0] === '/') {
        return path
    } else {
        return base.slice(0, base.lastIndexOf('/')) + '/' + path
    }
}

export function addBase(base: string, path: string): string {
    if(path[0] === '/' && base !== '/') {
        return base + path
    } else {
        return path
    }
}
