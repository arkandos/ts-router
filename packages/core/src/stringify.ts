import { Params, Pattern, SEPARATOR } from "./types"

/**
 * Turn params back into a (absolute) URL, given the pattern.
 *
 * Note that if your URL ccontains anonymous wildcards, these will always
 * be left blank.
 *
 * This function is intentionally a lot more lenient about handling
 * repeated/optional params, and isntead just looks at the type of the
 * param value. empty segments will be removed.
 *
 * The final URL will be encoded, such that it is safe to use anywhere.
 *
 * @param pattern The pattern to generate a URL for.
 * @param params The params to fill in the blanks
 * @returns The final URL with all variables replaced.
 */
export default function stringify(pattern: Pattern, params: Params): string {
    const relative = pattern
        .flatMap((segment) => {
            const value = segment.name
                ? params[segment.name]
                : segment.str

            if (!value) {
                return []
            } else if (Array.isArray(value)) {
                return value
            } else {
                return [value]
            }
        })
        .map(encodeURIComponent)
        .join(SEPARATOR)

    return '/' + relative
}
