import { resolve } from 'path'
import { defineConfig } from "vite"
import dts from 'vite-plugin-dts'

export default defineConfig({
    plugins: [
        dts()
    ],

    build: {
        lib: {
            entry: resolve(__dirname, 'lib/index.ts'),
            name: 'Router',
            fileName: 'router'
        },
        rollupOptions: {
            external: ['preact', 'preact/hooks'],
            output: {
                globals: {
                    'preact': 'preact',
                    'preact/hooks': 'preactHooks'
                }
            }
        }
    },
})
