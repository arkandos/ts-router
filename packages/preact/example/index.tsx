import { h, render, Fragment } from 'preact'
import { useCallback, useLayoutEffect, useRef, useState } from 'preact/hooks'

import { PathRouter, Route, Link, AsyncRoute } from '../lib'


function Form() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const usernameRef = useRef<HTMLInputElement|null>(null)
    useLayoutEffect(() => {
        usernameRef.current?.focus()
    }, [])


    return <>
        <form>
            <label>
                Username:
                <input
                    type="text"
                    autoFocus={true}
                    name="username"
                    value={username}
                    onInput={useCallback((e: Event) => setUsername((e.target as HTMLInputElement).value), [])}
                    ref={usernameRef}
                />
            </label>
            <br />
            <label>
                Password:
                <input
                    type="password"
                    name="password"
                    value={password}
                    onInput={useCallback((e: Event) => setPassword((e.target as HTMLInputElement).value), [])}
                />
            </label>
        </form>
        <p>
            Username: {username}<br />
            Password: {password}<br />
        </p>
        <p>
            <Link href="/form">Form 1</Link>
            <Link href="/form2">Form 2</Link>
        </p>
    </>
}

function Blog(props: { slug: string }) {
    return <>
        <h1>Blog Post</h1>
        <p>{props.slug}</p>
    </>
}

function App() {
    return (
        <PathRouter>
            <Route pattern="/">
                <h1>Hello, Sailor!</h1>
                <Link href="/async">async test</Link>
                <Link href="/form">form test</Link>
            </Route>

            <Route pattern="/blog/:slug" children={Blog} />

            <Route pattern="/form" children={Form} />
            <Route pattern="/form2" children={Form} />

            <AsyncRoute
                pattern="/async"
                component={() => import('./Async')}
                loading={<>Loading</>}
            />

            <Route pattern="/*">404</Route>
        </PathRouter>
    )
}

render(<App />, document.body)
