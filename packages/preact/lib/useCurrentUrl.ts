import { useEffect, useState } from "preact/hooks"
import { useRouter } from "./Router"

/**
 * Subscribe to URL changes.
 */
export default function useCurrentUrl(): string|null {
    const router = useRouter()

    const [url, setUrl] = useState(router.getUrl())
    useEffect(() => router.subscribe(setUrl), [router])

    return url
}
