import { createPathRouterWithBase, RouterUrl } from "@jreusch/router";
import { h, ComponentChildren } from "preact";
import { useEffect, useRef, useState } from "preact/hooks";
import Router from "./Router";

export default function PathWithBaseRouter(props: {
    children: ComponentChildren,
    base: string
    onChange?: (newUrl: RouterUrl, oldUrl: RouterUrl) => void,
}) {
    const [router, setRouter] = useState(() => createPathRouterWithBase(props.base))
    const oldBase = useRef(props.base)

    useEffect(() => {
        if (props.base !== oldBase.current) {
            setRouter(createPathRouterWithBase(props.base))
            oldBase.current = props.base
        }
    }, [props.base])

    return <Router children={props.children} router={router} onChange={props.onChange} />
}
