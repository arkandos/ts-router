import { createHashRouter, RouterUrl } from "@jreusch/router";
import { h, ComponentChildren } from "preact";
import { useState } from "preact/hooks";
import Router from "./Router";

export default function HashRouter(props: { children: ComponentChildren, onChange?: (newUrl: RouterUrl, oldUrl: RouterUrl) => void }) {
    const [router] = useState(createHashRouter())
    return <Router children={props.children} router={router} onChange={props.onChange} />
}
