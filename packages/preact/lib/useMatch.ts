import { match, Params, parse } from "@jreusch/router";
import { useMemo } from "preact/hooks";
import useCurrentUrl from "./useCurrentUrl";

/**
 * Subscribe to URL changes and update matched params whenever the url updates.
 */
export default function useMatch(pattern: string, allowPartial = false): Params|null {
    const currentUrl = useCurrentUrl()
    const parsedPattern = useMemo(() => parse(pattern), [pattern])
    const params = useMemo(() => {
        if (currentUrl) {
            return match(parsedPattern, currentUrl, allowPartial)
        } else {
            return null
        }
    }, [parsedPattern, currentUrl, allowPartial])
    return params
}
