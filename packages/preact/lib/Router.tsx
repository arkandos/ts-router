import type { Router, RouterUrl } from "@jreusch/router"
import { ComponentChildren, createContext, h } from "preact"
import { useContext, useEffect, useRef, useState } from "preact/hooks"

export interface RouterContext {
    matched: string
    router: Router
}


function createRouterContext(router: Router): RouterContext {
    return {
        matched: '',
        router
    }
}

const Context = createContext<RouterContext|null>(null)

export function useRouterContext() {
    return useContext(Context)!
}

/**
 * Provides access to the router object for programmatic navigation.
 *
 * Only available inside of a `<Router>` component tree.
 */
export function useRouter(): Router {
    return useRouterContext().router
}

export interface RouterProps {
    children: ComponentChildren

    /**
     * Router to use.
     * Needs to be provided to support bundle-splitting.
     */
    router: Router,

    /**
     * Optional callback to listen to URL changes.
     */
    onChange?: (to: RouterUrl, from: RouterUrl) => any
}

/**
 * Top-level component; should wrap all `<Route>` components in some way.
 * It is ensured that only 1 pattern can match inside a `<Router>` tree.
 */
export default function Router(props: RouterProps) {
    const [context, setContext] = useState(() => createRouterContext(props.router))
    useEffect(() => {
        if(props.router === context.router) {
            return
        }
        setContext(createRouterContext(props.router))
    }, [props.router])

    const onChange = useRef(props.onChange)
    useEffect(() => {
        onChange.current = props.onChange
    }, [props.onChange])

    const previousUrl = useRef(context.router.getUrl())

    useEffect(() => props.router.subscribe((url: string|null) => {
        context.matched = ''
        if (onChange.current) {
            onChange.current(url, previousUrl.current)
        }
        previousUrl.current = url
    }), [context])

    return <Context.Provider value={context} children={props.children} />
}
