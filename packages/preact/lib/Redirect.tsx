import { getUrl } from "@jreusch/router"
import { useRouterContext } from "./Router"
import useMatch from "./useMatch"

export interface RedirectProps {
    /**
     * Pattern to match against.
     * Matched params will be replaced in the `to` pattern.
     */
    from: string
    /**
     * Pattern to navigate to.
     * Params will be filled in by matches from the `from` pattern.
     */
    to: string

    /**
     * Replace the current history entry instead of doing a "true" redirect.
     *
     * @default true
     */
    replace?: boolean
}

/**
 * Redirect is a render-less component watching the current url, automatically
 * navigating to another URL if it matches.
 *
 * @example
 * <Redirect from="/impressum" to="/about-us" />
 * <Redirect from="/blog/:slug+" to="/posts/:slug+" />
 */
export default function Redirect(props: RedirectProps) {
    const ctx = useRouterContext()
    const match = useMatch(props.from)

    // only redirect if no other route matches
    if (match && !ctx.matched) {
        ctx.router.navigate(getUrl(props.to, match), props.replace ?? true)
    }

    return null
}
