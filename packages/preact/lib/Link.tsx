import { getUrl, Params } from '@jreusch/router';
import { ComponentChildren, h } from 'preact'
import { useRouter } from './Router';
import useMatch from './useMatch'

export interface LinkProps {
    /**
     * Link target URL. Should start with a slash ('/') to enable client-side navigation.
     */
    href: string
    /**
     * If set, interpret the href as a pattern, and use stringify to build the
     * final URL set on the <a> tag.
     */
    params?: Params,
    /**
     * Class to always set on the <a> tag.
     */
    className?: string
    /**
     * Class only set if the current URL matches the Link's href.
     * See `exact` for details.
     */
    activeClassName?: string
    /**
     * Class only set if the current URL does not match the Link's href.
     * See `exact` for details.
     */
    inactiveClassName?: string
    /**
     * By default, any URL that starts with the href will, set the link into
     * `active` state. Using `exact`, you can change that behaviour to instead
     * only match if the URL is exactly the same as in the href.
     */
    exact?: boolean

    children: ComponentChildren
}

/**
 * A Link is like an <a>, but dynamically adds and removes classes based on the URL.
 *
 * @example
 * <Link href="/impressum">Impressum</Link>
 * <Link
 *      href="/blog/:slug+"
 *      params={{ slug: ['hello-world'] }}
 * >
 *      Previous post
 * </Link>
 * <Link
 *      href="/blog"
 *      className="block px-4 py-2"
 *      activeClassName="bg-blue-600 text-white"
 *      inactiveClassName="text-gray-900 hover:bg-gray-200"
 * >
 *      My ramblings
 * </Link>
 */
export default function Link(props: LinkProps) {
    const router = useRouter()
    const url = getUrl(props.href, props.params)
    const href = router.toHref(url)

    const onClick = (evt: MouseEvent) => {
        if (evt.shiftKey || evt.metaKey || evt.ctrlKey || evt.altKey || evt.button) {
            return
        }

        evt.preventDefault()
        router.navigate(url)
    }

    const matches = null !== useMatch(url, !props.exact)

    let className = props.className || ''
    if (matches && props.activeClassName) {
        className += ' ' + props.activeClassName
    } else if (!matches && props.inactiveClassName) {
        className += ' ' + props.inactiveClassName
    }

    return (
        <a href={href} className={className} onClick={onClick}>
            {props.children}
        </a>
    )
}

