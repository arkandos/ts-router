import type { Params } from "@jreusch/router"
import { ComponentChildren, ComponentType, createContext, h } from "preact"
import { useContext, useState } from "preact/hooks"

import { useRouterContext } from './Router'
import useMatch from "./useMatch"


export interface RouteProps<T extends Params = Params> {
    /**
     * Pattern to match against.
     * The extracted params will be available using `useParams`, and will be
     * automatially set as props if the children are a single component.
     */
    pattern: string

    /**
     * Children to render if the pattern matches.
     * In addition to providing "normal" component children, you can also
     * provide a component constructor (or function) directly, which will be
     * called using the params as props.
     */
    children: ComponentChildren | ComponentType<T>
}



const ParamsContext = createContext<Params|null>(null)

/**
 * An alternative to providing a function to `<Route>`, you can also use this function
 * to inject the params into the component.
 *
 * Note that this only works inside of the `<Route>`.
 */
export function useParams<T extends Params = Params>(): T {
    const context = useContext(ParamsContext)
    return context as T
}

/**
 * Use the `<Route>` component to wrap your pages, rendering them conditionally
 * depending on the current URL.
 *
 * You can provide a function (e.g. a component) to dynamically render content based on the matched params.
 *
 * To use bundle-splitting and load Route components dynamically, see `<AsyncRoute>`.
 *
 * @example
 * <Route pattern="/">Home</Route>
 * <Route pattern="/impressum" children={Impressum} />
 */
export function Route<T extends Params = Params>(props: RouteProps<T>) {
    const ctx = useRouterContext()
    const params = useMatch(props.pattern)

    if (!params) { // no match
        return null
    }

    if (ctx.matched && ctx.matched !== props.pattern) { // match but not first
        return null
    }

    ctx.matched = props.pattern

    const children = props.children
    return <ParamsContext.Provider value={params}>
        { isComponent(children)
            ? h(children, params)
            : children
        }
    </ParamsContext.Provider>
}

export interface AsyncRouteProps<T> {
    /**
     * Pattern to match against.
     * The extracted params will be available using `useParams`, and will be
     * automatially set as props on the loaded component.
     */
    pattern: string
    /**
     * Function to load the component.
     * Should usually call `import(...)` to enable bundle-splitting.
     * The function itself is called with the params to enable dynamic imports.
     */
    component: (params: T) => Promise<{ default: ComponentType<T> }>
    /**
     * Alternative content to show while the promise is loading.
     */
    loading?: ComponentChildren | ComponentType<T>
}

/**
 * Asynchronously load a (default-exported) component to use as a route.
 * Params will be automatically set as props on the resolved component.
 *
 * @example
 * <AsyncRoute
 *      pattern="/blog/:slug+"
 *      component={() => import('./Blog.tsx')}
 *      loading="Loading..."
 *  />
 */
export function AsyncRoute<T extends Params>(props: AsyncRouteProps<T>) {
    const ctx = useRouterContext()
    const params = useMatch(props.pattern)

    let [component, setComponent] = useState<ComponentType<T>|null>(null)

    if (!params) { // no match
        return null
    }

    if (ctx.matched && ctx.matched !== props.pattern) { // match but not first
        return null
    }

    if (!ctx.matched) { // first match
        ctx.matched = props.pattern
        component = null
        setComponent(null)
        props.component(params as T).then(({ default: component }) => {
            setComponent(() => component)
        })
    }

    return <ParamsContext.Provider value={params}>
        { component
            ? h(component, params as T)
            : isComponent(props.loading)
            ? h(props.loading, params as T)
            : props.loading
        }
    </ParamsContext.Provider>
}

function isComponent(x: any): x is ComponentType {
    return typeof x === 'function'
}
