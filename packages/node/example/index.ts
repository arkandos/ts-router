import {
    get,
    post,
    put,
    delet,
    group,
    wrap,
    compile
} from '@jreusch/router-node'

import { createServer, type IncomingMessage, type ServerResponse } from 'node:http'
import { serve, send, json } from 'micro'

const PORT = parseInt(process.env.PORT || '', 10) || 3000

interface User {
    id: number,
    name: string,
    bio: string
}

let nextUserId = 6
const users = [
    {
        id: 1,
        name: "John Doe",
        bio: "I'm a tech enthusiast and love to explore new technologies.",
    },
    {
        id: 2,
        name: "Jane Smith",
        bio: "Passionate about art and photography. Always looking for new inspiration.",
    },
    {
        id: 3,
        name: "Mike Johnson",
        bio: "An avid traveler and foodie. I believe in experiencing different cultures.",
    },
    {
        id: 4,
        name: "Emily Brown",
        bio: "Nature lover and outdoor enthusiast. Hiking is my favorite weekend activity.",
    },
    {
        id: 5,
        name: "Alex Lee",
        bio: "A software developer with a passion for creating useful and innovative apps.",
    },
]

const dispatch = compile(
    (req, res) => req.method,
    (req, res) => new URL(req.url, `http://${req.headers.host}`).pathname,

    wrap(async (next, params, req, res) => {
        const start = Date.now()
        try {
            return await next(params, req, res)
        } finally {
            console.log(req.method, req.url, ' -- ', res.statusCode, ', took', Date.now() - start, 'ms')
        }
    },

    get('/', async (params, req: IncomingMessage, res: ServerResponse<IncomingMessage>) => {
        // annotate the return type as unknown here -
        // micro can handle (almost) anything we return to it
        return 'Hello, World!' as unknown
    }),

    group('/users',
        get('', async (params, req, res) => {
            return users as unknown
        }),

        post('', async (params, req, res) => {
            const user: User = await json(req) as User
            user.id = nextUserId++
            users.push(user)
            return user as unknown
        }),

        group('/:userId',
            get('', async (params, req, res) => {
                const userId = parseInt(`${params.userId}`, 10)
                const user = users.find(user => user.id === userId)
                if (user) {
                    return user
                } else {
                    return send(res, 404)
                }
            }),

            put('', async (params, req, res) => {
                const userId = parseInt(`${params.userId}`, 10)
                const newData: User = await json(req) as User

                let user = users.find(user => user.id === userId)
                if (user) {
                    user.name = newData.name || user.name
                    user.bio = newData.bio || user.bio
                }

                return user as unknown
            }),

            delet('', async (params, req, res) => {
                const userId = parseInt(`${params.userId}`, 10)
                const index = users.findIndex(user => user.id === userId)

                users.splice(index, 1)

                send(res, 201)
            })
        )
    ))
)


const server = createServer(serve(async (req, res) => {
    const resultPromise = dispatch(req, res)
    if (resultPromise === null) {
        return send(res, 404)
    }

    return await resultPromise
}))

server.listen(PORT, () => {
    console.log('Server started, listening on port', PORT)
})
