import { parse, match as matchPattern, type Params } from '@jreusch/router'

// as per usual, the full @jreusch/router API is also exposed.
export * from '@jreusch/router'


/**
 * Handler is the callback function you provide that gets called if a route matches.
 *
 * A handler is any function that takes in the params as its first argument, followed
 * by all (generic) custom arguments that you provided to dispatch.
 *
 * All Handler in a single Router tree need to match up in their arguments and
 * return types.
 */
export interface Handler<TArgs extends any[], TRet> {
    (params: Params, ...args: TArgs): TRet
}

interface RouterCtx<TArgs extends any[]> {
    method: string
    pathname: string
    params: Params
    args: TArgs
}

interface Router<TArgs extends any[], TRet> {
    (ctx: RouterCtx<TArgs>): TRet|null
}

/**
 * Create a Router handling GET requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * get('/users/:userId', getUser)
 */
export function get<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('GET', pattern, handler)
}

/**
 * Create a Router handling HEAD requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * head('/users/:userId', getUserHeaders)
 */
export function head<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('HEAD', pattern, handler)
}

/**
 * Create a Router handling POST requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * post('/users', createUser)
 */
export function post<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('POST', pattern, handler)
}

/**
 * Create a Router handling PUT requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * put('/users/:userId', replaceUser)
 */
export function put<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('PUT', pattern, handler)
}

/**
 * Create a Router handling DELETE requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * delet('/users/:userId', deleteUser)
 */
export function delet<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('DELETE', pattern, handler)
}

/**
 * Create a Router handling CONNECT requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * connect('/users/:userId', watchUser)
 */
export function connect<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('CONNECT', pattern, handler)
}

/**
 * Create a Router handling OPTIONS requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * options('*', getCORS)
 */
export function options<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('OPTIONS', pattern, handler)
}

/**
 * Create a Router handling TRACE requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * trace('/', getDebugInfo)
 */
export function trace<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('TRACE', pattern, handler)
}

/**
 * Create a Router handling PATCH requests.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * patch('/users/:userId', updateUser)
 */
export function patch<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    return on('PATCH', pattern, handler)
}

/**
 * Create a Router handling requests to a custom HTTP method.
 *
 * `get`, `post`, `put`, etc. are all implemented as simple wrappers around this function.
 *
 * @param method the method that needs to match.
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * on('LIST', '/users', listUsers)
 */
export function on<TArgs extends any[], TRet>(method: string, pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    method = method.toUpperCase()

    const inner = all(pattern, handler)
    return (ctx) => {
        if (ctx.method !== method) {
            return null
        }
        return inner(ctx)
    }
}

/**
 * Create a Router handling all HTTP methods, matching only the pattern.
 *
 * After checking the method, `on` just forwards wraps this router.
 *
 * @param pattern The (partial) pattern to match.
 * @param handler Your callback that gets called when the pattern matches
 *
 * @example
 * all('/users/:userId', userController.run)
 */
export function all<TArgs extends any[], TRet>(pattern: string, handler: Handler<TArgs, TRet>): Router<TArgs, TRet> {
    const parsed = parse(pattern)
    return (ctx) => {
        const params = matchPattern(parsed, ctx.pathname)
        if (params === null) {
            return null
        }

        return handler({ ...ctx.params, ...params }, ...ctx.args)
    }
}


/**
 * Combine multiple routers into a single one, trying them all sequentially until
 * the first router matches.
 *
 * All functions taking a router as arguments also allow you to pass multiple routers,
 * so most of the time, you don't need to call this function manually in practice.
 *
 * @param routes the other routers to try in order
 *
 * @example
 * routes(
 *     post('/users', createUser),
 *     get('/users/:userId', getUser),
 *     put('/users/:userId', updateUser),
 *     delet('/users/:userId', deleteUser)
 * )
 */
export function routes<TArgs extends any[], TRet>(...routes: Array<Router<TArgs, TRet>>): Router<TArgs, TRet> {
    if (routes.length === 1) {
        return routes[0] // exactly 1 route - no need to wrap
    }

    return (ctx) => {
        for (const route of routes) {
            const result = route(ctx)
            if (result !== null) {
                return result
            }
        }

        return null
    }
}

/**
 * Greedily match a `prefix` pattern, only trying the given routers when the prefix matches.
 *
 * The prefix pattern can contain variables, which will be merged into the final params passed to the handlers.
 *
 * It *cannot* contain optional segments, since that would mean that the route could match if we backtracked, breaking our assumption that we can match the prefix in a greedy fashion.
 *
 * Nested routers match against the suffix only.
 *
 * The routers internally are combined using `routes` into a single router.
 *
 * @param base the pattern for the prefix
 * @param routers a list of nested routers matching against the suffix
 *
 * @example
 * // This is equivalent to the example given for `routes`
 * group('/users',
 *     post('', createUser),
 *     group(':userId',
 *         get('', getUser),
 *         put('', updateUser),
 *     ),
 *     // it is not required that the nested path is empty!
 *     delet('"userId', deleteUser)
 * )
 */
export function group<TArgs extends any[], TRet>(base: string, ...routers: Array<Router<TArgs, TRet>>): Router<TArgs, TRet> {
    const inner = routes(...routers)
    const parsed = parse(base)
    if (!parsed.length) {
        return inner // empty base - same as routes
    }

    // make sure we do not have wildcard segments
    // repeated or optional segments would mean we might backtrack while matching.
    // This would mean that we cannot greedily match against the base anymore!
    if (parsed.some(segment => segment.optional || segment.repeated)) {
        throw new Error(`group prefixes (base) are not allowed to contain optional or repeated segments!`)
    }

    return (ctx) => {
        const params = matchPattern(parsed, ctx.pathname, true)
        if (params === null) {
            return null
        }

        return inner({
            ...ctx,

            params: { ...ctx.params, ...params },

            // we now matched parsed.length segments, where we know that every
            // segment matched exactly 1 part of the path.
            pathname: ctx.pathname
                .split('/')
                .filter(x => x)
                .slice(parsed.length)
                .join('/')
        })
    }
}


/**
 * Create a Router filtering another router on additional properties that are not
 * the method or the pathname.
 *
 * This can be used for example to implement content negotation using the `Accept` header.
 *
 * @param pattern The (partial) pattern to match.
 * @param routers The routes to filter
 *
 * @example
 * filter((req) => req.headers.accept === 'application/json', apiRoutes)
 */
export function filter<TArgs extends any[], TRet>(predicate: (...args: TArgs) => boolean, ...routers: Array<Router<TArgs, TRet>>): Router<TArgs, TRet> {
    const inner = routes(...routers)
    return (ctx) => {
        if (!predicate(...ctx.args)) {
            return null
        }
        return inner(ctx)
    }
}


/**
 * Transform the arguments.
 *
 * @param f transform the final arguments into simpler ones
 * @param routers Inner routers taking the simpler arguments
 *
 * @example
 * mapArgs((req) => [req.body] as const, ...routes)
 */
export function mapArgs<TArgsIn extends any[], TArgsOut extends any[], TRet>(f: (...args: TArgsOut) => TArgsIn, ...routers: Array<Router<TArgsIn, TRet>>): Router<TArgsOut, TRet> {
    const inner = routes(...routers)
    return (ctx) => inner({ ...ctx, args: f(...ctx.args) })
}

/**
 * Transform the result.
 *
 * @param f transform a simpler return type into the final response
 * @param routers Inner routers that return the simpler type
 *
 * @example
 * mapRet(body => new Response(body, { status: 200 }), ...routes)
 */
export function mapRet<TArgs extends any[], TRetIn, TRetOut>(f: (value: TRetIn) => TRetOut, ...routers: Array<Router<TArgs, TRetIn>>): Router<TArgs, TRetOut> {
    const inner = routes(...routers)
    return (ctx) => {
        const result = inner(ctx)
        if (result === null) {
            return null
        }
        return f(result)
    }
}


/**
 * Continue doing something else.
 *
 * This is the most flexible combinator in this library,
 * allowing you to basically hijack the whole thing, implement additional filters,
 * preprocessing, middleware, promises, and more.
 *
 * @param f A middleware function. Takes in the same arguments as a Handler, but the first parameter is an additional "next" callback.
 * @param routers The routers to wrap
 *
 * @example
 * // exception handling
 * wrap((next, params, req) => {
 *     try {
 *         return next(params, req)
 *     } catch(err) {
 *         console.error('[ERROR]', req.method, req.url, ' -- ', err.message)
 *         return new Response(err.message, { status: 500 })
 *     }
 * }, ...routes)
 *
 * @example
 * // server timing
 * wrap(async (next, params, ...args) => {
 *     const start = Date.now()
 *
 *     const res = await next(params, ...args)
 *     if (res === null) {
 *         return null
 *     }
 *
 *     const dur = Date.now() - start
 *     res.headers.add('Server-Timing', `total;dur=${dur}`)
 *     return res
 * }, ...routes)
 *
 * @example
 * // authorization middlware
 * wrap(async (next, params, req) => {
 *     const auth = req.headers.get('Authorization')
 *     const user = await authenticate(auth)
 *      if (!user) {
 *          return new Response('Unauthorized', { status: 401 })
 *      }
 *      // add the user the the arguments of the nested routes
 *      return await next(params, user, req)
 * }, ...routes)
 *
 * @example
 * // fetch common resources based on url params
 * // the order of group/wrap is important to get the params!
 * group('/users/:userId', wrap(async (next, params, req) => {
 *     const userId = parseInt(params.userId, 10)
 *     const user = await getUser(userId)
 *     if (!user) {
 *         return new Reponse('Not found', { status: 404 })
 *     }
 *     // inner handlers have the type (params: Params, user: User) => Promise<Response>
 *     return await next(params, user)
 * }, ...routes))
 */
export function wrap<TArgsIn extends any[], TRetIn, TArgsOut extends any[] = TArgsIn, TRetOut = TRetIn>(
    f: (next: (params: Params, ...args: TArgsIn) => TRetIn|null, params: Params, ...args: TArgsOut) => TRetOut|null,
    ...routers: Array<Router<TArgsIn, TRetIn>>
): Router<TArgsOut, TRetOut> {
    const inner = routes(...routers)
    return ctx => {
        return f((params, ...args) => inner({ ...ctx, params, args }), ctx.params, ...ctx.args)
    }
}


/**
 * Take a router and turn it into a `dispatch` function which you can call with your
 * custom arguments.
 *
 * For the router to work, you need to give it a way to extract the method,
 * and a way to extract the pathname.
 *
 * The given routers are then combined using the `routes` into a single router.
 *
 * @param getMethod How to get the method from the arguments?
 * @param getPathname How to get the pathname from the arguments?
 * @param routers A list of routers to match against
 * @returns A dispatch function, which you can call on just your arguments. The dispatcher will extract the method and pathname using your provided functions, and will then try to find a matching route. If it finds one, it calls the associated handler. If not, `null` is returned.
 *
 * @example
 * compile(
 *     (req) => req.method,
 *     (req) => new URL(req.url).pathname,
 *     apiRoutes
 * )
 */
export function compile<TArgs extends any[], TRet>(
    getMethod: (...args: TArgs) => string,
    getPathname: (...args: TArgs) => string,
    ...routers: Array<Router<TArgs, TRet>>
): (...args: TArgs) => TRet|null {
    const router = routes(...routers)
    return (...args) => {
        const method = getMethod(...args).toUpperCase()
        const pathname = getPathname(...args)

        return router({
            args,
            method,
            pathname,
            params: {},
        })
    }
}
